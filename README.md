# Wardrobify

Team:

-   Anthony Woo - Shoes Microservice
-   Andrew Huang - Hats Microservice

## Design

Main Page, Shoes, and Hats services will all include the main navbar to access those endpoints respectively.

### Shoes Design Requirements

!['design requirements for shoes microservice'](/assets/shoe_reqs.png)

### Hats Design Requirements

!['design requirements for hats microservice'](/assets/hats_reqs.png)

## Shoes microservice

The models involve with the shoes microservice include a "Shoe" model as well as a "BinVO" model. The BinVO model instances are created based on polled data from the wardrobe-api service. The BinVO instances are critical for the Shoe model to have a bin property via foreign key relationship. On the react frontend, the shoes microservice can be accessed via the links in the Main nav bar. There is an additional navigation bar component to implement links to and from the list view and shoeForm view.

### API Paths localhost:8080

-   "/api/shoes/" to see list of shoes or to create an instance["GET", "POST"]
-   "/api/shoes/{id}" to delete, get details, or update an instance of shoes by id ["GET", "DELETE", "PUT"]

### Front End Paths localhost:3000

-   "/" Home Page
-   "/shoes/" List of shoes
-   "/shoes/new" Create new instance of shoe

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
