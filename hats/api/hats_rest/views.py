from django.shortcuts import render
from .models import Hat, LocationVO
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href",]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric","color","style_name","picture_url","id",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.id}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name", "color","picture_url", "location",]
    encoders = {"location": LocationVODetailEncoder(),}


@require_http_methods(["GET", "POST"])
def api_list_hats(request) :
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, HatListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"]=location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(hat, HatDetailEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_hat_detail(request,pk):
    if request.method =="GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat,encoder=HatDetailEncoder,safe=False,)
    elif request.method =="PUT":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_href = content["location"]
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location id"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat, HatDetailEncoder, False)
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})
