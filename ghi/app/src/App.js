import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './Shoes.js';
import ShoeForm from './ShoeForm.js';
import HatsList from './Hats.js';
import HatForm from './HatForm.js';

function App() {
    const [shoes, setShoes] = useState([]);
    const [bins, setBins] = useState([]);
    const [hats, setHats] = useState([]);
    const [locations, setLocations] = useState([]);

    const fetchShoes = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    };
    const fetchBins = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    };

    const fetchHats = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    };
    const fetchLocations = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    };

    useEffect(() => {
        fetchShoes();
        fetchBins();
        fetchHats();
        fetchLocations();
    }, []);

    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="shoes">
                        <Route
                            index
                            element={
                                <ShoesList
                                    shoes={shoes}
                                    fetchShoes={fetchShoes}
                                />
                            }
                        />
                        <Route
                            path="new"
                            element={
                                <ShoeForm bins={bins} fetchShoes={fetchShoes} />
                            }
                        />
                    </Route>

                    <Route path="hats">
                        <Route index element=
                                {<HatsList
                                    hats={hats}
                                    fetchHats={fetchHats}
                                />}
                        />
                        <Route path="new" element={<HatForm locations={locations} />} />
                        </Route>
                    </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
