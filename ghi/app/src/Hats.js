import React from 'react';
import HatNav from './HatNav.js';

function ListHats({ hats, fetchHats}) {
    const deleteHat = async (hat) => {
        const url = `http://localhost:8090/api/hats/${hat.id}`;
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try{
            const response = await fetch(url,fetchConfig)
            if (response.ok) {
                fetchHats();
            }
        } catch(error) {
            console.log('error deleting')
        }
    };

    return (
        <>
            <HatNav />
            <div>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Fabric</th>
                            <th>Style</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Location</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {hats.map((hat) => {
                            return (
                                <tr key={hat.id}>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.style_name}</td>
                                    <td>{hat.color}</td>
                                    <td>
                                        <img
                                            src={hat.picture_url}
                                            style={{ height: 100, width: 100 }}
                                        ></img>
                                    </td>
                                    <td>{hat.location}</td>
                                    <td>
                                        <button onClick={() => deleteHat(hat)}>
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    )
}
export default ListHats;
