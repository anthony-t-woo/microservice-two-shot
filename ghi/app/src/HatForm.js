import React, { useEffect, useState } from 'react';
import HatNav from './HatNav.js';

function HatForm({ locations, fetchHats }) {
    const [location, setLocation] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');

    const handleLocationChange = (evt) => {
        const value = evt.target.value;
        setLocation(value);
    };
    const handleFabricChange = (evt) => {
        const value = evt.target.value;
        setFabric(value);
    };
    const handleStyleChange = (evt) => {
        const value = evt.target.value;
        setStyle(value);
    };
    const handleColorChange = (evt) => {
        const value = evt.target.value;
        setColor(value);
    };
    const handlePictureChange = (evt) => {
        const value = evt.target.value;
        setPicture(value);
    };

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        const data = {
            fabric: fabric,
            style_name: style,
            color: color,
            picture_url: picture,
            location: location,
        };
        console.log(data);
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        };
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url, fetchOptions);

        if (response.ok) {
            const newHat = await response.json();
            setLocation('');
            setFabric('');
            setStyle('');
            setColor('');
            setPicture('');
        }
    };

    return (
        <>
            <HatNav />
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form
                            id="create-conference-form"
                            onSubmit={handleSubmit}
                        >
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFabricChange}
                                    placeholder="fabric"
                                    required
                                    value={fabric}
                                    name="fabric"
                                    type="text"
                                    id="fabric"
                                    className="form-control"
                                />
                                <label htmlFor="fabric">
                                    Fabric
                                </label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleStyleChange}
                                    placeholder="style"
                                    required
                                    value={style}
                                    name="style"
                                    type="text"
                                    id="style"
                                    className="form-control"
                                />
                                <label htmlFor="style">style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleColorChange}
                                    placeholder="Color"
                                    required
                                    value={color}
                                    name="color"
                                    type="text"
                                    id="color"
                                    className="form-control"
                                />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handlePictureChange}
                                    placeholder="Picture"
                                    required
                                    value={picture}
                                    name="picture"
                                    type="text"
                                    id="picture"
                                    className="form-control"
                                />
                                <label htmlFor="picture">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={handleLocationChange}
                                    required
                                    name="location"
                                    id="location"
                                    className="form-select"
                                    value={location}
                                >
                                    <option value="">Select a location</option>
                                    {locations.map((location) => (
                                        <option key={location.id} value={location.href}>
                                            location # {location.id} in {location.closet_name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default HatForm;
