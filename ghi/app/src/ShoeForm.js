import React, { useEffect, useState } from 'react';
import ShoeNav from './ShoeNav.js';

function ShoeForm({ bins, fetchShoes }) {
    const [bin, setBin] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [model, setModel] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');

    const handleBinChange = (evt) => {
        const value = evt.target.value;
        setBin(value);
    };
    const handleManufacturerChange = (evt) => {
        const value = evt.target.value;
        setManufacturer(value);
    };
    const handleModelChange = (evt) => {
        const value = evt.target.value;
        setModel(value);
    };
    const handleColorChange = (evt) => {
        const value = evt.target.value;
        setColor(value);
    };
    const handlePictureChange = (evt) => {
        const value = evt.target.value;
        setPicture(value);
    };

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        const data = {
            manufacturer: manufacturer,
            model_name: model,
            color: color,
            picture_url: picture,
            bin: bin,
        };
        console.log(data);
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        };
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url, fetchOptions);

        if (response.ok) {
            const newShoe = await response.json();
            fetchShoes();
            setBin('');
            setManufacturer('');
            setModel('');
            setColor('');
            setPicture('');
        }
    };

    return (
        <>
            <ShoeNav />
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form
                            id="create-conference-form"
                            onSubmit={handleSubmit}
                        >
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleManufacturerChange}
                                    placeholder="Manufacturer"
                                    required
                                    value={manufacturer}
                                    name="manufacturer"
                                    type="text"
                                    id="manufacturer"
                                    className="form-control"
                                />
                                <label htmlFor="manufacturer">
                                    Manufacturer
                                </label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleModelChange}
                                    placeholder="Model"
                                    required
                                    value={model}
                                    name="model"
                                    type="text"
                                    id="model"
                                    className="form-control"
                                />
                                <label htmlFor="model">Model</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleColorChange}
                                    placeholder="Color"
                                    required
                                    value={color}
                                    name="color"
                                    type="text"
                                    id="color"
                                    className="form-control"
                                />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handlePictureChange}
                                    placeholder="Picture"
                                    required
                                    value={picture}
                                    name="picture"
                                    type="text"
                                    id="picture"
                                    className="form-control"
                                />
                                <label htmlFor="picture">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={handleBinChange}
                                    required
                                    name="bin"
                                    id="bin"
                                    className="form-select"
                                    value={bin}
                                >
                                    <option value="">Select a bin</option>
                                    {bins.map((bin) => (
                                        <option key={bin.id} value={bin.href}>
                                            Bin # {bin.id} in {bin.closet_name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ShoeForm;
