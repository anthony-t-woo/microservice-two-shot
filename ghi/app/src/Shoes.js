import React from 'react';
import ShoeNav from './ShoeNav.js';

function ShoesList({ shoes, fetchShoes }) {
    console.log(shoes);
    const deleteShoe = async (shoe) => {
        const url = `http://localhost:8080/api/shoes/${shoe.id}`;
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                fetchShoes();
            }
        } catch (error) {
            console.log('Big Problems');
        }
    };

    return (
        <>
            <ShoeNav />
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                            <th>Model</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Bin</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {shoes.map((shoe) => {
                            return (
                                <tr key={shoe.id}>
                                    <td>{shoe.manufacturer}</td>
                                    <td>{shoe.model_name}</td>
                                    <td>{shoe.color}</td>
                                    <td>
                                        <img
                                            src={shoe.picture_url}
                                            style={{ height: 150, width: 150 }}
                                        ></img>
                                    </td>
                                    <td>
                                        Bin # {shoe.bin} in {shoe.closet_name}
                                    </td>
                                    <td>
                                        <button
                                            onClick={() => deleteShoe(shoe)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default ShoesList;
